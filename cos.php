<!DOCTYPE html>

<?php include "functions.php"; ?>


<html lang="en">
<head>
    <?php include 'bootstrap/bootstrap.php' ?>
    <link rel="stylesheet" type="text/css" href="style.css">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta charset="UTF-8">
    <title>Shop </title>
</head>

<body>

<?php include 'parts/header.php' ?>
<?php
$data = query('
SELECT cart.id, cart.quantity, products.name as namep, products.price, products.image, category.name as namec, products.price*cart.quantity as valoare
FROM (cart left join products on cart.products_id = products.id) left join category on products.category_id = category.id'
);

$dataTOTAL = query('
SELECT sum(quantity) as t1, sum(price) as t2, sum(quantity*price) as t3
FROM (cart left join products on cart.products_id = products.id)'
);
?>


<div style="width: 80%; margin: auto">
    <div id="basketTop" style="text-align: center; font-family: ptsans_regular,arial,sans-serif;">
        <h3><strong>COSUL TAU</strong></h3>
        <div class="freeDeliveryInfo" style="background-color: #FF9B00; height: 50px; padding: 10px">
            <p>COMENZILE PLASATE INAINTE DE FINALUL ANULUI VOR PUTEA FI RETURNATE PANA PE 31.01.2020</p>
        </div>
    </div>
    <br>

    <div id="content" style="border-radius: 10px">
        <?php
        echo "<div><a style='margin-top:60px;' href='cos_add.php'><strong><del>Adauga toate produsele (17 produse)</del></strong></a></div>";
        echo "<div><a href='cos_tru.php'><strong><del>Sterge toate produsele</del></strong></a></div>";
        echo "<div class='row'>";
        echo "<div class='col-sm-5' style='color: gray; margin-left: 10px'><strong>PRODUS</strong></div>";
        echo "<div class='col-sm-2' style='color: gray'><strong>DETALII</strong></div>";
        echo "<div class='col-sm-2' style='color: gray'><strong>CANTITATE</strong></div>";
        echo "<div class='col-sm-1' style='color: gray'><strong>PRET</strong></div>";
        echo "<div class='col-sm-1' style='color: gray'><strong>TOTAL</strong></div>";
        echo "</div>";
        $i = 0;
        foreach ($data as $line => $value) {
            echo "<div class='card' style='margin: 5px; border-radius: 10px;'>";
            echo "<div class='card-body'>";
            echo "<div class='row'>";
            echo "<div class='col-sm-2' style='color: gray'>";
            echo "<a style='float:left; margin-top:60px; margin-right:15px' href='cos_del.php?id=" . $value['id'] . "'><strong>X</strong></a>";
            echo "<img style='float:left; width:100px; margin:5px' src='./Poze/$value[image]' alt='Card image cap'/>";
            echo "</div>";
            echo "<div class='col-sm-3'>$value[namep]</div>";
            echo "<div class='col-sm-2'>$value[namec]</div>";
//            echo "<div style='text-align: center; width: 10px' class='col-sm-2'><input id='CANTITATE' size='2' onkeyup='myFunction(this.value)' type='text' value='$value[cantitate]'></div>";
            ?>
            <div class='col-sm-2'>
                <form action="cos_mod.php?id=<?php echo $value['id']; ?>" method="post">
                    <table>
                        <tr>
                            <td>
                                <input onchange="this.form.submit();" size='1' type="text" name="cantitate"
                                       value="<?php echo $data[$i]['quantity']; ?>"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <?php
            echo "<div class='col-sm-1'>$value[price] LEI</div>";
            $v = number_format($value['valoare'], 2, '.', '');
            echo "<div id='TOTAL' class='col-sm-1'><strong>$v LEI</strong></div>";
            echo "</div>";
            echo "</div>";
            echo "</div>";
            $i++;
        }
        //       echo "<div style='clear:both'></div>";
        $t1=$dataTOTAL[0]['t1'];
        $t2=number_format($dataTOTAL[0]['t2'], 2, '.', '');
        $t3=number_format($dataTOTAL[0]['t3'], 2, '.', '');
        ?>
        <div class='row'>
            <div class='col-sm-5' style='color: gray; margin-left: 10px'></div>
            <div class='col-sm-2' style='color: darkgreen' ><strong>TOTAL</strong></div>
            <div class='col-sm-2' style='color: darkgreen' ><strong><?php echo $t1 ?></strong></div>
            <div class='col-sm-1' style='color: darkgreen' ><strong><?php echo $t2.' LEI' ?></strong></div>
            <div class='col-sm-1' style='color: darkgreen' ><strong><?php echo $t3.' LEI' ?></strong></div>
        </div>
    </div>

    <br>
    <div id="wish" class="clearfix">
        <a href="wishlist.php" class="viewSave">WISHLIST-UL MEU?</a>
    </div>
    <br>
    <br>
    <br>
    <div>
        <a href="index.php" id="continueShopping">Continuă cumpărăturile</a>
    </div>
    <br>
</div>

</body>
</html>
