<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">

<?php

include  'functions.php';
include 'bootstrap/bootstrap.php';
include 'parts/header.php'; ?>
</head>
<body>

<div class="container h-100">
    <div class="d-flex justify-content-center">
        <div class="card mt-5 col-md-4 animated bounceInDown myForm">
            <div class="card-header">
                <h4>Add Category</h4>
            </div>
            <div class="card-body">
                <form  method="post" action="ProcessCreateCategory.php">
                    <div id="dynamic_container">
                        <div class="input-group mt-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text br-15"><i class="fa fa-list-alt"></i></span>
                            </div>
                            <input type="text" placeholder="" name="category" id="categoryInput" class="form-control"/>
                            <button type="submit"><i class="fas fa-plus-circle"></i> Add</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </div>
</div>
</body>