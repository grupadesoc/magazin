<?php
class Product
{
    public $id;
    public $category_id;
    public $name;
    public $image;
    public $stock;
    public $price;

    /**
     * Product constructor.
     * @param $id
     * @param $id_category
     * @param $name
     * @param $image
     * @param $stock
     * @param $price
     */
    public function __construct($id)
    {
        $this->id = $id;

        $data = query("SELECT * FROM products WHERE id=" . intval($this->id));

        $this->id_category = $data[0]['category_id'];
        $this->name = $data[0]['name'];
        $this->image = $data[0]['image'];
        $this->stock = $data[0]['stock'];
        $this->price = $data[0]['price'];
    }

}
