<?php

class Wishlist
{
public $productId;
public $userId;

    /**
     * Wishlist constructor.
     * @param $productId
     * @param $categoryId
     * @param $userId
     */
    public function __construct($id)
    {

        
		$this->id = $id;
        $data = query("SELECT * FROM wishlist WHERE id=".intval($this->id));
        $this->userId = $data[0]['userId'];;
        $this->productId = $data[0]['productId'];



    }
public function getProduct() {
    return new Product($this->productId);
}
 
    /**
     * @return mixed
     */
    public function getCategory($categoryId)
    {
        return new Category($categoryId);
    }
}