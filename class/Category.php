<?php

class Category extends Base
{

    public $name;


    public function getName()
    {
        return $this->name;
    }
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    public function getProducts($limit, $pagenumber)
    {$offset = ($pagenumber-1)*$limit;
        $data = $this::findBy([],null,"",$limit,$offset);
        $list = [];

        foreach ($data as $dbLine){
            $list[] = new Product($dbLine['id']);
        }

        return $list;
    }


}