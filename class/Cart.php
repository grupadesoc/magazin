<?php

/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 1/6/2020
 * Time: 7:43 PM
 */
class Cart
{
    public $id;

    public $products_id;

    public $quantity;

    public $user_id;

    /**
     * Product constructor.
     * @param $id
     * @param $title
     * @param $price
     * @param $description
     * @param $categoryId
     */
    public function __construct($id)
    {
        $this->id = $id;

        $data = query("SELECT * FROM cart WHERE id=".intval($this->id));

        $this->products_id = $data[0]['products_id'];
        $this->quantity = $data[0]['quantity'];
        $this->user_id = $data[0]['user_id'];
    }
    public function getProducts()
    {
        return new Product($this->products_id);
    }
}