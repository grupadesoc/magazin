<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php
    include '../../bootstrap/bootstrap.php';
    include '../header.php';
    ?>
    <style>
        .text-justify {
            font-size: 13px;
        }

        .text-sm-left {
            font-weight: bold;
        }
    </style>
</head>
<body>
<div class="container">
    <p class="text-sm-left">I. INTRODUCERE</p>
    <p class="text-justify">1. Wearco SA cu sediul în Cracovia, adresa: 31-564 Cracovia, str. Aleja Pokoju 18,
        înregistrată
        în Registrul Naţiona Judiciar KRS de pe lângă Tribunalul Districtual Cracovia, Secţia a XI –a Economică KRS, sub
        numărul KRS 0000816066, Identificator REGON 122515020, Cod fiscal NIP 6793080390, cu un capital social de 733
        550,00
        zloți, facilitează achiziţionarea de mărfuri cu ajutorul reţelei electronice (Internet) – de la adresa:
        www.SHOP.RO („MAGAZIN”). Administratorul datelor personale ale clienţilor Magazinului este societatea Wearco SA
        cu sediul în Cracovia, 31-564 Cracovia, str. Aleja Pokoju 18</p>
    <p class="text-justify">2. Prezentul Regulament se adresează tuturor utilizatorilor Magazinului şi stabileşte
        principiile de efectuare a înregistrărilor şi de utilizare a contului de Magazin, principiile de rezervare pe
        cale
        electronică a mărfurilor disponibile în Magazin (“MĂRFURI” sau „MARFA”), de plasare de comenzi pe site-ul
        Magazinului precum şi principiul încheierii contractelor de vânzare a Mărfurilor.</p>
    <p class="text-justify">3. Poate accesa Regulamentul orice utilizator al Magazinului, în orice moment, “dând click”
        pe
        linkul „Termeni si Conditii” aflat pe pagina de web a magazinului şi prin salvarea acestuia în format PDF pe
        suportul de informaţii dorit. Acesta se va afla la adresa http://188.240.210.8/web-04/adrian/AdrianShop/parts/FooterContent/TermeniSiConditii.php</p>
    <p class="text-justify">4. Informaţiile despre Mărfurile din Magazin – printre altele, descrieri, preţuri –
        constituie o
        invitaţie la încheierea de contracte de vânzare în sensul legilor în vigoare, conform condiţiilor din documentul
        Termeni şi Condiții.

    </p>
    <p class="text-justify">5. Mărfurile din Magazin sunt prezentate în mod amănunţit. Pe pagina de web se află
        informaţii
        detaliate despre proprietăţile Mărfii, preţul acesteia, materialul din care este realizată etc.

    </p>
    <p class="text-sm-left">II. REGULILE DE UTILIZARE A MAGAZINULUI ŞI DE ȊNCHEIERE DE CONTRACTE DE VÂNZARE A MĂRFURILOR

    </p>
    <p class="text-justify">1. Wearco SA facilitează încheierea de contracte de vânzare a Mărfurilor prin intermediul
        internetului şi prstează o serie de servicii, prevăzute în Regulamentul de faţă.

    </p>
    <p class="text-justify">2. Contractul de vânzare a mărfurilor se încheie între utilizatorul Magazinului („CLIENT”),
        şi Wearco SA cu sediul în Cracovia.

    </p>
    <p class="text-justify">4. Condiţia începerii utilizării Magazinului este cunoaşterea acestui Regulament şi
        acceptarea sa.

    </p>
    <p class="text-justify">5. Informaţiile furnizate de Client în timpul plasării comenzii trebuie să fie reale,
        actuale şi exacte. Wearco SA îşi rezervă dreptul de a refuza realizarea comenzii, în cazul când datele furnizate
        nu sunt suficient de exacte şi fac imposibilă realizarea comenzii, mai exact, nu permit predarea corespunzatoare
        a produselor. Înainte de a refuza realizarea comenzii Wearco SA va încerca să îl contacteze pe Client pentru a
        stabili ce date sunt necesare pentru a face posibilă realizarea acesteia.

    </p>
    <p class="text-justify">6. Magazinul ia toate măsurile tehnice şi administrative posibile şi impuse de anumite norme
        juridice, menite a proteja datele personale ale Clienţilor şi mai ales de a preveni obţinerea şi modificarea de
        către persoane neautorizate a datelor personale furnizate în timpul înregistrării.

    </p>
    <p class="text-justify">7. Clientul care a utilizat Magazinul are obligaţiile:

    </p>
    <p class="text-justify">a) de a nu furniza şi transmite informaţii interzise prin lege;

    </p>
    <p class="text-justify">b) de a utiliza Magazinul într-un mod care să nu perturbe funcţionarea acestuia;

    </p>
    <p class="text-justify">c) de a nu difuza şi de a nu plasa în cadrul Magazinului informaţii comerciale incorecte;

    </p>
    <p class="text-justify">d) de a utiliza Magazinul într-un mod care să nu creeze inconveniente pentru alţi clienţi şi
        pentru Administratorul Magazinului;

    </p>
    <p class="text-justify">e) de a folosi informaţiile amplasate în paginile magazinului doar în scop personal.

    </p>

    <p class="text-sm-left">III. ȊNCHEIEREA CONTRACTELOR DE VÂNZARE:

    </p>
    <p class="text-justify">1. Magazinul facilitează plasarea comenzilor de Marfă după cum urmează:

    </p>
    <p class="text-justify">a) pe pagina web a Magazinului (on-line), urmând procedura de plasare a comenzilor,

    </p>
    <p class="text-justify">b) telefonic, prin contact cu infolinia Magazinului la numărul de telefon afisat pe website
        in sectiunea Contact

    </p>
    <p class="text-justify">c) prin e-mail, transmiţând comanda cu Mărfurile alese pe adresa: crm@answear.ro

    </p>
    <p class="text-justify">2. Magazinul primeşte comenzile plasate on-line pe parcursul întregii zile, în toate zilele
        săptămânii. Comenzile plasate prin telefon sau prin poştă electronică sunt primite în zilele lucrătoare între
        orele 9.00 – 18.00. Comenzile plasate în zilele libere sau de sărbători vor fiprelucrate în decurs de două zile
        lucrătoare de la data plasării comenzii

    </p>
    <p class="text-justify">3. Clientul poate plasa o comandă fără a fi nevoie de a-şi înregistra permanent datele
        personale în baza de date a Magazinului (aşa numitele cumpărături fără înregistrare).

    </p>
    <p class="text-justify">4. Condiţia plasării comenzii o constituie completarea în formular a tuturor datelor cerute,
        necesare pentru expediere şi pentru generarea din sistem a facturii fiscale sau bonului fiscal.

    </p>
    <p class="text-justify">5. Pentru a plasa o comandă se aleg Mărfurile din Magazin, mai ales în ceea ce priveşte
        cantitatea, culoarea, mărimea, după care se trece la “COŞ” şi continuă procedura de plasare a comenzii
        selectându-se diferitele opţiuni. Codurile de reducere nu pot fi combinate cu alte programe de loialitate și
        parteneriat disponibile în magazinul ANSWEAR.ro și nu este posibilă beneficierea de mai multe programe în timpul
        unei tranzacții.

    </p>
    <p class="text-justify">6. Până la momentul confirmării selectării Mărfurilor cu tasta „Trimite comanda” Clientul
        are posibilitatea de a efectua modificări referitoare la Marfa din comandă, precum şi la datele pentru livrare
        şi de facturare. Confirmarea de către Client a comenzii prin tasta „Finalizează comanda” este echivalentă cu
        acceptarea obligaţiei de a face plata preţului Mărfurilor şi a costurilor de livrare, lucru despre care Clientul
        este informat direct, înainte de confirmarea comenzii.

    </p>
    <p class="text-justify">7. Confirmarea de către Client a comenzii conform pct. 6 de mai sus constituie oferta
        transmisă de Client către Wearco SA, de a încheia contractul de vânzare, conform conţinutului comenzii şi
        prezentului Regulament.

    </p>
    <p class="text-justify">8. In momentul plasării comenzii conform pct. 6 de mai sus, Clientul primeşte la adresa de
        e-mail indicată un mesaj referitor la comanda plasată: cantităţile de Mărfuri comandate, valoarea comenzii,
        tipul de livrare şi de plată ales, timpul de livrare a comenzii precum şi datele de contact ale Clientului,
        Magazinului, informaţii despre modul de depunere a reclamaţiei în privinţa Mărfurilor precum şi despre dreptul
        Clientului de a se retrage din contractul de vânzare. Procedura aceasta reprezintă şi o confirmare a primirii de
        către Magazin a ofertei de achiziţie din partea Clientului.

    </p>
    <p class="text-justify">9. Magazinul transmite confirmarea acceptului sau refuzul de a accepta comanda Clientului,
        transmisă prin e-mail sau telefonic (Confirmarea realizării şi livrării comenzii) la adresa de e-mail indicată
        de acesta. După primirea confirmării de mai sus se ajunge la încheierea între Client şi Wearco SA a contractului
        de vânzare a Mărfurilor comandate de Client.

    </p>
    <p class="text-justify">10. Contractul de vânzare se încheie în limba română, având conţinutul prezentulului
        Regulament completat cu comanda plasată de Client.

    </p>
    <p class="text-justify">11. Fără atingerea dreptului Clientului de se retrage din contract, conform legislaţiei în
        vigoare, Clientul poate renunţa la comandă înainte de a primi de la Magazin confirmarea ofertei de achiziţie,
        adică înainte de a primi mailul care confirm acceptarea comenzii spre realizare, despre care se vorbeste la pct.
        9 de mai sus. În această privinţă Clientul trebuie să contacteze urgent legătura cu Magazinul – este posibil
        prin telefon, cu confirmare prin e-mail.

    </p>
    <p class="text-justify">12. In cazul alegerii de către Client a altei forme de plată decât “ramburs” Magazinul îşi
        rezervă dreptul de a refuza realizarea comenzii în cadrul contractului de vânzare, în cazul când: (i) datele de
        contact ale Cumpărătorului sunt atât de neclare, încât fac imposibilă livrarea mărfii, (ii) tranzacţia nu a fost
        autorizată în sistemul de plăţi electronice sau (iii) plata pentru comandă nu a fost efectuată în termen de 2
        zile lucrătoare de la data plasării comenzii.

    </p>
    <p class="text-sm-left">IV. LIVRAREA ŞI RECEPȚIONAREA MĂRFURILOR

    </p>
    <p class="text-justify">1. Mărfurile sunt livrate la adresa indicată de către Client în comandă. Termenul estimativ
        pentru livrarea Mărfurilor este stabilit în rezumatul comenzii, înainte de confirmarea de către Client a
        plasării comenzii prin tasta "Trimite comanda”.</p>
    <p class="text-justify">2. Timpul prevăzut pentru livrarea la Client, la livrarea prin curier, este de 5 zile
        lucrătoare din ziua următoare trimiterii coletului. Termenul integral, maxim, de realizare a comenzii nu ar
        trebui să depăşească 7 zile lucrătoare şi în niciun caz nu va depăşi 20 zile de la data încheierii contractului
        de vânzare. Începerea realizării comenzii poate fi întârziată până la momentul înregistrării în contul bancar al
        Magazinului al preţului vânzării (şi eventualelor costuri cu livrarea), în cazul alegerii de către Client al
        formei de plată în avans prin internet (adică prin serviciul de plată online sau printr-un transfer on-line
        obişnuit).</p>
    <p class="text-justify">3. Mărfurile sunt livrate la adresa indicată de pe teritoriul României de către firma de
        expeditie DPD România sau Fan Courier. Taxele pentru livrarea produselor se indică în procesul de plasare a
        comenzii. Magazinul facilitează şi preluarea mărfurilor în magazine staţionare, indicate la pct. 6 şi
        următoarele.

    </p>
    <p class="text-justify">4. Toate trimiterile sunt asigurate de firma de curierat DPD România sau Fan Courier.

    </p>
    <p class="text-justify">5. Comenzile cu o valoare de peste 200,00 (în litere: două sute) lei vor fi expediate pe
        cheltuiala Magazinului. În celelalte cazuri costul de expediere a Mărfurilor achiziţionate, în cuantum de 9 (în
        litere: nouă) lei pentru colete de pana in 3 kg si de 12,9 (în litere: doisprezece virgulă nouă) lei este
        suportat de către Client, lucru despre care este informat tot înainte de confirmarea comenzii plasate. La
        fiecare produs expediat se ataşează dovada achiziţionării (bon fiscal sau factură fiscală) precum şi un formular
        de înlocuire/returnare a mărfii.

    </p>
    <p class="text-sm-left">V. PREȚURI ŞI METODE DE PLATĂ</p>
    <p class="text-justify">1. Informaţia despre preţul de achiziţie al mărfii de pe pagina de web a magazinului este
        obligatorie din momentul primirii de către Client a unui e-mail de confirmare a acceptării comenzii plasate de
        acesta, pentru achiziţia Mărfurilor selectate, conform pct. II par. 9. Respectivul preţ nu mai suferă
        modificări, indiferent de modificările de preţ din Magazin care mai pot avea loc după confirmarea comenzii prin
        mail.

    </p>
    <p class="text-justify">2.. Preţurile produselor din magazine sunt date în lei şi conţin toate componentele
        preţului, inclusiv TVA, taxe vamale şi impozite.

    </p>
    <p class="text-justify">3. Clientul va achita preţul Produselor comandate şi cheltuielile de expediţie, după cum
        doreşte:

    </p>
    <p class="text-justify">a) “prin ramburs”, achitând firmei de curierat la livrare

    </p>
    <p class="text-justify">b) plata prin card prin intermediul serviciului autorizat mobilpay.ro, înainte de livrare.
        În cazul în care Clientul alege plata în avans ca metodă de plată, lipsa plății pentru contul WearCo SA sau
        intermediarului de tranzacții (serviciul mobilpay.ro), în termen de 2 zile de la plasarea comenzii, va duce la
        anularea comenzii. În acest caz, puteți plasa din nou o comandă și puteți alege o altă metodă de plată.
        Procesarea comenzii plătite prin plată electronică începe după primirea plății pentru bunuri.

    </p>
    <p class="text-justify">4. Wearco SA îşi rezervă dreptul de a modifica preţul mărfurilor aflate în Magazin, de a
        introduce în vânzare mărfuri noi, de a desfăşura sau de a revoca acţiuni de promovare pe pagina de web a
        Magazinului, de a introduce pe aceasta modificări conforme cu prevederile legislatiei în vigoare, cu condiţia ca
        aceste modificări să nu încalce drepturile persoanelor care au înncheiat contracte de vânzare a Mărfurilor
        oferite de Magazin înainte de efectuarea respectivelor modificări, sau a drepturilor persoanelor autorizate să
        utilizeze a anume promoţie, conform principiilor acesteia, pe perioada desfăşurării sale.

    </p>
    <p class="text-sm-left">VI. RECLAMAȚII REFERITOARE LA MĂRFURI

    </p>
    <p class="text-justify">1. Produsele oferite prin magazine sunt noi şi originale. Wearco SA răspunde pentru
        defectele fizice sau juridice ale Mărfurilor pe baza legislației în vigoare. De asemenea Wearco SA respectă
        garanția legală pentru conformitatea produselor.

    </p>
    <p class="text-justify">2. WearCo SA va lua măsuri pentru a asigura pe deplin funcţionarea corectă a Magazinului, în
        domeniile indicate de cunoştinţele tehnice actuale şi se obligă să înlăture într-un termen rezonabil toate
        neajunsurile semnalizate de către Clienţi.

    </p>
    <p class="text-justify">3. Orice lucru cumpărat în Magazin poate fi obiectul unei reclamaţii, cu respectarea
        termenilor şi condiţiilor de reclamaţie stabiliţi prin lege, dacă prezintă defecte ce constituie neconformităţi
        cu contractul de vânzare încheiat.

    </p>
    <p class="text-justify">5. Reclamaţiile se pot trimite prin poştă, returnând marfa împreună cu descrierea în scris a
        defectului sau cu un formular de reclamaţie descărcat de pe pagina Magazinului şi cu dovada achiziţionării, prin
        scrisoare recomandată sau altă altă formă de transmitere, la adresa: Sklep Internetowy ANSWEAR.RO Cracovia
        (30-527), str. Na Zjeździe 11, adăugând: „Reclamatie magazin online ANSWEAR.RO”. Clientul va fi informat despre
        modul de rezolvarea a reclamaţiei în termen de 14 zile, calculate din ziua următoare primirii de către Magazin a
        trimiterii cu marfa reclamată.

    </p>
    <p class="text-justify">6. Clientul are dreptul la următoarele pretenţii, conform legislației în vigoare:

    </p>
    <p class="text-justify">a) Să ceară să renunţe la Contract sau să ceară reducerea preţului Mărfii, dacă Vânzătorul
        nu înlocuieşte de urgenţă şi fără mari inconveniente pentru Cumpărător, marfa defectă sau nu înlătură defectul,
        ori

    </p>
    <p class="text-justify">b) Să ceară înlocuirea Mărfii cu o alta fără defecte sau înlăturarea defectelor.

    </p>
    <p class="text-justify">7. În cazul în care reclamaţia nu este admisă, Marfa va fi returnată împreună cu un raport
        referitor la nejustificarea reclamaţiei.

    </p>

    <p class="text-justify">8. In cazul în care se constată de către Client deteriorarea Mărfii în timpul transportului
        se recomandă ca el să întocmească în prezenţa curierului un proces verbal de daune.

    </p>
    <p class="text-sm-left">VII. RETURURILE DE MARFĂ – RETRAGEREA DIN CONTRACTUL DE VÂNZARE

    </p>

    <p class="text-justify">1. Clientul – consumator, așa cum este definit conform normelor în vigoare, are dreptul de a
        se retrage din contractul de vânzare fără a indica motivul, pe baza dispozițiilor legale și a principiilor de
        mai jos.

    </p>
    <p class="text-justify">2. Termenul de retragere din contractul de vânzare a Mărfurilor expiră după împlinirea a 30
        zile de la data când Clientul a intrat în posesia mărfurilor sau când o a treia persoană, alta decât
        transportatorul, indicată de către Client, a intrat în posesia Mărfurilor.

    </p>
    <p class="text-justify">3. Pentru a-şi exercita dreptul de retragere din contract Clientul trebuie să aducă la
        cunoştinţa Magazinului decizia sa de renunţare, printr-o declaraţie clară (de ex. O scrisoare expediată prin
        poştă, fax sau e-mail) transmisă pe adresa: FAN COURIER S.A. CALEA BODROGULUI NR. 18 COD POSTAL 310059 ARAD,
        ROMANIA cu mențiunea "ANSWEAR.RO - RETUR".

    </p>
    <p class="text-justify">4. Clienţii pot folosi modelul de formular de retragere din contract care vi se pune la
        dispoziţie prin e-mail imediat după confirmarea comenzii si care exista si pe website, dar acest lucru nu este
        obligatoriu. Clienţii pot, de asemenea, să completeze şi să trimită scanat formularul sau orice alte pretenţii
        prin e-mail, la adresa: crm@ANSWEAR.ro. Dacă un Client foloseşte această modalitate, Magazinul îi va trimite de
        urgenţă confirmarea primirii informaţiei despre retragerea din contract pe un suport de informaţii durabil (de
        ex. prin poştă electronică).

    </p>
    <p class="text-justify">5. Pentru a respecta termenul de retragere din contract este suficient ca Clientul să
        trimită informaţiile referitoare la exercitarea dreptului de retragere înainte de împlinirea termenului de
        renunţare.

    </p>
    <p class="text-justify">6. Clientul trimite mărfurile care fac obiectul retragerii din contract la următoarea adresă
        poştală: FAN COURIER S.A. CALEA BODROGULUI NR. 18 COD POSTAL 310059 ARAD, ROMANIA cu mențiunea "Answear.ro -
        Retur" de îndată, sau, în orice caz, nu mai târziu de 30 zile de la data când a informat Magazinul despre
        retragerea din prezentul contract. Termenul se consideră a fi respectat dacă Clientul returnează obiectul
        înainte de împlinirea termenului de 30 zile.

    </p>
    <p class="text-justify">7. Clientul suportă costurile directe de returnare a mărfurilor.

    </p>
    <p class="text-justify">8. In cazul retragerii din contract Magazinul va returna Clienţilor sumele încasate,
        inclusiv costurile de livrare a produselor (cu excepţia celor cheltuielilor suplimentare rezultate din faptul că
        a fost ales de Client un alt mod de livrare decât cel standard, oferit de Magazin), de îndată, sau nu mai târziu
        de 14 zile de la data când Magazinul a fost informat despre decizia Clientului de a-şi exercita dreptul de
        renunţare la contract.

    </p>
    <p class="text-justify">9. Returnarea sumelor achitate de Client se face în acelaşi mod cu cel ales de Client la
        tranzacţia iniţială, dacă Clientul nu şi-a exprimat acordul pentru alte soluţii; în niciun caz, Clientul nu
        suportă taxele legate de acest retur. Magazinul poate amână returnarea plăţii până la momentul primirii
        produsului sau până la momentul livrării la Magazin a dovezii că acesta a fost expediat înapoi, în funcţie de
        care din cele 2 fapte se petrece primul.

    </p>
    <p class="text-justify">10. Clienţii răspund doar pentru reducerea valorii Mărfurilor returnate, rezultate din
        utilizarea acestora în alt mod decât cel necesar pentru a constata caracterul, trăsăturile şi funcţionarea
        produsului.

    </p>
    <p class="text-justify">11. Clienţii care deţin cont în Magazin (au fost înregistraţi), au făcut o singură dată
        cumpărături de Mărfuri în calitate de Clienţi logaţi, în valoare de peste 200 lei şi au renunţat la contractual
        de vânzare a Mărfurilor, pot beneficia de returnarea gratuită (numită în continuare” RETURNARE GRATUITA /DARMOWY
        ZWROT”) pe baza următoarelor principii:

    </p>
    <p class="text-justify">a) pentru a beneficia de opţiunea Returnării Gratuite este necesar să se locheze la contul
        din Magazin., să intre în rubrica „RETURURI/ZWROTY” şi să procedeze conform celor indicate acolo,

    </p>
    <p class="text-justify">b) Retururile Gratuite pot avea loc în termen de 30 zile de la expedierea Mărfurilor la
        Client

    </p>
    <p class="text-justify">c) Retururile Gratuite sunt posibile pe teritoriul Romaniei.

    </p>
    <p class="text-justify">12. Opţiunea Retururilor Gratuite este un drept al Clientului care îndeplineşte condiţiile
        din par. 9 de mai sus şi nu îngrădeşte Clientul în dreptul său de a renunţa la contractual de vânzare a
        Mărfurilor în termen de 30 zile de la primirea lor, conform par. 8 de mai sus. Clientul care îndeplineşte
        condiţiile pentru a beneficia de opţiunea Retururi Gratuite poate returna produsele în baza regulilor generale,
        la alegere.

    </p>
    <p class="text-sm-left">VIII. INFORMATII REFERITOARE LA SERVICIILE PRESTATE ON-LINE

    </p>
    <p class="text-justify">1. Magazinul prestează pentru Clienţi următoarele servicii, pe cale electronică:

    </p>
    <p class="text-justify">a) Facilitarea încheierii on-line a contractelor de vânzare din Magazin, conform prezentului
        Regulament,

    </p>
    <p class="text-justify">b) Facilitarea deschiderii de conturi ale Clienţilor în Magazin,

    </p>
    <p class="text-justify">c) Transmiterea informaţiei comerciale comandate, referitoare la mărfuri.

    </p>
    <p class="text-justify">2. Clientul are dreptul de a se retrage din contractul de vânzare a mărfurilor conform
        normelor juridice aplicabile şi în baza principiilor stabilite de prezentul Regulament. In plus, Clientul are
        dreptul, în orice moment, de a cere încetarea de către Magazin a prestării serviciilor menţionate la pct 1spct
        b) şi c) de mai sus.

    </p>
    <p class="text-justify">3. Condiţiile tehnice de prestare de către Magazin, pe cale electronica, a serviciilor, sunt
        următoarele:

    </p>
    <p class="text-justify">a) acces la Internet,

    </p>
    <p class="text-justify">b) folosirea unui browser care face posibiliă editare unor documente hypertext

    </p>
    <p class="text-justify">(Internet Explorer, Opera, FireFox< Chrome sau altele asemănătoare),

    </p>
    <p class="text-justify">c) Deţinerea unui cont de poştă electronică.

    </p>
    <p class="text-justify">4. Reclamaţii privitoare la serviciile prestate de Magazin pe cale electronică pot fi depuse
        transmiţându-le la adresa de e-mail a Biroului de Customer Service a Magazinului crm@ANSWEAR.ro sau telefonic,
        la numerele de telefon afisate pe website. Reclamaţia Clientului trebuie să conţină denumirea beneficiarului şi
        o scurtă descriere a procesului reclamat. Magazinul se va strădui ca reclamaţiile depuse să fie examinate în cel
        mai scurt timp cu putinţă, totuşi, nu mai târziu de 14 zile de la data primirii reclamaţiei de către Magazin.
        Despre modul în care a fost soluţionată reclamaţia Clientul va fi anunţat pe calea aleasă de Client, telefonic
        sau ca informaţie transmisă pe e-mail la adresa indicată de acesta.

    </p>
    <p class="text-justify">Wearco SA aduce la cunoştinţă că în funcţie de setările browserului Clientului poate
        introduce în sistemul teleinformatic al Clientului fişiere cookies, ce nu sunt o componentă a conţinutului
        serviciilor prestate de către Magazin dar care ar putea permite identificarea ulterioară a Clientului ce intră
        pe pagina de web a Magazinului şi sunt utilizate de Magazin pentru a înlesni Clientului utilizarea Magazinului,
        ca şi pentru monitorizarea de către Magazin a navigărilor Clientului pe pagina de web. Clientul poate renunţa în
        orice moment la posibilitatea ca Magazinul să utilizeze cookies, prin anumite setări ale browserului său.

    </p>
    <p class="text-sm-left">IX. ALTE PREVEDERI

    </p>
    <p class="text-justify">1. Conținutul și design-ul Answear.ro, inclusiv look&feel-ul acestuia și bazele de date
        accesibile prin intermediul său, sunt proprietatea Answear.ro și sunt protejate prin legislația în vigoare cu
        privire la drepturile de autor și drepturile conexe. În cazul informațiilor și conținutului postat de terțe
        părți pe site-ul Answear.ro, dreptul de autor și responsabilitatea asupra acestora aparțin în totalitate celor
        care au publicat acea informație.

    </p>
    <p class="text-justify">2. Puteți copia și printa conținutul Answear.ro doar pentru folosința dumneavoastră
        personală, în scopuri necomerciale.

    </p>
    <p class="text-justify">3. Este interzisă orice utilizare a conținutului Answear.ro în alte scopuri decât cele
        permise expres de prezentul document sau de legislația în vigoare. Cererile de utilizare a conținutului în alte
        scopuri decât cele permise expres de prezentul document pot fi trimise la adresa de email.

    </p>
    <p class="text-justify">În condițiile în care considerați că un anumit conținut aflat pe site-ul Answear.ro încalcă
        drepturile dvs. de proprietate intelectuală, dreptul la viața privată, la publicitate sau alte drepturi
        personale, sunteți rugat să trimiteți un e-mail la contact at Answear.ro cu drepturile încălcate pentru a
        permite administratorilor Answear.ro să acționeze în conformitate cu dispozițiile legale.

    </p>
    <p class="text-justify">4. Forța majoră. Answear.ro, partenerii sau utilizatorii săi nu pot fi făcuți responsabili
        pentru nici o întârziere sau eroare în executarea obligațiilor contractuale sau în conținutul furnizat pe
        site-ul nostru, rezultând direct sau indirect din cauze care nu depind de voința Answear.ro. Această exonerare
        include, dar nu se limitează la: erorile de funcționare al echipamentului tehnic de la Answear.ro, lipsa
        funcționării conexiunii la internet, lipsa funcționării conexiunilor de telefon, virușii informatici, accesul
        neautorizat în sistemele Answear.ro, erorile de operare, greva etc.

    </p>
    <p class="text-sm-left"> X INFORMAȚII REFERITOARE LA PRELUCRAREA DE CĂTRE WEARCO SA A DATELOR CU CARACTER PERSONAL
        ALE UTILIZATORILOR MAGAZINULUI

    </p>
    <p class="text-justify"> 1. Informațiile indicate mai jos sunt, de asemenea, puse la dispoziția Utilizatorului în
        momentul colectării datelor sale cu caracter personal pe site-ul Magazinului.

    </p>
    <p class="text-justify">2. Operatorul datelor cu caracter personal ale Utilizatorilor Magazinului este societatea
        Wearco Societate pe actiuni. Operatorul prelucrează datele cu caracter personal ale utilizatorilor Magazinului
        în conformitate cu Regulamentul general privind protecția datelor din 27 aprilie 2016 ("GDPR").

    </p>
    <p class="text-justify">3. Datele de contact la responsabilul cu protecția datelor: dpo@wearco.pl sau: Responsabilul
        pentru Protecția Datelor al Wearco Societate pe Actiuni, ul. Na Zjeździe 11, 30 – 527 Kraków.

    </p>
    <p class="text-justify">4. Datele cu caracter personal ale Utilizatorilor Magazinului sunt prelucrate, printre
        altele în următoarele domenii: (i) pentru a executa contractele de vânzare a bunurilor de la Magazin încheiate
        cu Utilizatorul – temeiul pentru prelucrarea datelor va fi în acest caz contractul încheiat cu Operatorul prin
        aprobarea Regulamentului Magazinului; (ii) pentru a menține un cont al Utilizatorului Magazinului – temeiul
        pentru prelucrarea datelor va fi în acest caz contractul încheiat cu Operatorul prin crearea unui cont și
        acceptarea Regulamentului Magazinului; (ii) pentru a desfășura procedurile de reclamație – în acest caz, temeiul
        pentru prelucrare este obligația Operatorului care rezultă din dispoziția legii legată de garanția pentru
        defecțiunile articolului vândut; (Iii) în cazul în care Utilizatorul și-a dat acordul separat, la adresa de
        e-mail specificată a Utilizatorului sau la numărul său de telefon, pot fi transmise informații comerciale
        privind bunurile oferite spre vânzare în Magazin, inclusiv oferte promoționale – în acest caz, temeiul pentru
        prelucrarea datelor Utilizatorului este consimțământul acestuia, care nu este obligatoriu și poate fi retras în
        orice moment; (iv) pentru a trimite mesaje de marketing personalizate Utilizatorului pe site-ul Magazinului, de
        exemplu, sub forma unei sugestii de a cumpăra bunuri utilizând profilarea. Mesajele vor fi pregătite în baza
        analizei cumpărăturilor efectuate de Utilizator – temeiul pentru prelucrarea datelor Utilizatorului, în acest
        caz, va fi interesul legitim al Operatorului care se bazează pe comercializarea bunurilor oferite în magazin;
        (v) în scopuri de marketing – la adresa pentru corespondență furnizată de către Utilizator, Operatorul din când
        în când poate să trimită informații despre oferta Magazinului Answear.com sau ofera partenerilor comerciali ai
        acestuia – temeiul pentru prelucrarea datelor cu caracter personal în acest sens va fi interesul legitim al
        Operatorului sau al partenerilor săi constând în comercializarea bunurilor indicate în ofertă; Utilizatorul
        poate, în orice moment, să se opună prelucrării datelor sale cu caracter personal în acest sens prin contactarea
        Magazinului, (vi) în scopuri statistice pentru nevoile interne ale Operatorului – în acest caz, temeiul pentru
        prelucrare va fi interesul legitim al Operatorului care se bazează pe colectarea informațiilor care fac posibilă
        dezvoltarea activității și ajustarea serviciilor la nevoile Utilizatorilor Magazinului, (vii) pentru a confirma
        executarea de către Operator a obligațiilor sale și pentru a înainta pretenții sau pentru a se apăra împotriva
        pretențiilor care pot fi înaintate Operatorului, pentru a preveni sau detecta frauda – temeiul pentru
        prelucrarea datelor Utilizatorului, în acest caz, va fi interesul legitim al Operatorului, care este protejarea
        drepturilor, confirmarea executării obligațiilor și obținerea remunerației datorate de la clienții
        Operatorului.</p>
    <p class="text-justify">5. La efectuarea cumpărăturilor în Magazin, Utilizatorul furnizează anumite date personale
        necesare pentru a executa contractul de vânzare. Refuzul de a furniza datele necesare executării comenzii are
        drept rezultat incapacitatea Magazinului de a executa contractul de vânzare. Nu este obligatoriu ca Utilizatorul
        să consimtă să primească informații comerciale la adresa de e-mail furnizată sau la numărul de telefon indicat
        pentru executarea contractului de vânzare a bunurilor încheiat. Dacă este exprimat consimțământul, acesta poate
        fi retras în orice moment. Retragerea consimțământului nu afectează legalitatea prelucrării înainte de
        retragere.

    </p>
    <p class="text-justify">6. Operatorul va pune datele cu caracter personal ale Utilizatorului la dispoziția
        entităților care colaborează cu noi în executarea contractului de vânzare a bunurilor achiziționate de
        Utilizator, inclusiv în primirea plății pentru bunurile achiziționate, precum și în livrarea de bunuri.

    </p>
    <p class="text-justify">7. Operatorul utilizează instrumente de securitate informatică și măsuri organizatorice care
        au ca scop minimizarea riscului scurgerii de date, al distrugerii, dezintegrării acestora, cum ar fi sistemul de
        firewall, sisteme de securitate antivirus și anti-spam, procedurile interne de acces, de prelucrare a datelor și
        de recuperare în caz de dezastru, precum și sistem de backup care rulează pe multe niveluri. Magazinul oferă un
        nivel foarte ridicat de securitate prin utilizarea Web application firewall (WAF) și sistem de securitate
        împotriva atacurilor DDoS, un nivel ridicat de criptare HTTPS / SSL, în conformitate cu cele mai bune practici
        acceptate, cooperează cu furnizori de hosting selectat cu atenție care posedă certificate în domeniul
        managementului calității ISO 9001 și îndeplinesc cerințele AQAP-2110, precum și certificatul de management al
        securității informațiilor conform standardului ISO / IEC 27001.

    </p>
    <p class="text-justify">8. Pe principiile stabilite în GDPR Utilizatorul are următoarele drepturi în ceea ce
        privește prelucrarea datelor cu caracter personal de către Operator în legătură cu operarea Magazinului: dreptul
        de acces la date, dreptul de a le actualiza, dreptul de a solicita transferul de date, ștergerea acestora,
        dreptul de a se opune prelucrării datelor și dreptul de a cere limitarea procesării acestora.

    </p>
    <p class="text-justify">9. Datele cu caracter personal furnizate de Utilizator vor fi prelucrate în timpul necesar
        pentru a executa contractul de vânzare și cererile de reclamație, precum și pentru confirmarea executării
        obligațiilor Operatorului și înaintarea pretențiilor sau apărarea împotriva pretențiilor care pot fi înaintate
        Operatorului – dar nu mai mult de 10 ani de la data furnizării către Operator de către Utilizator a datelor
        sale.

    </p>
    <p class="text-justify">9. Pe principiile stabilite în GDPR Utilizatorul are următoarele drepturi în ceea ce
        privește prelucrarea datelor cu caracter personal de către Operator în legătură cu operarea Magazinului: dreptul
        de acces la date, dreptul de a le actualiza, dreptul de a solicita transferul de date, ștergerea acestora,
        dreptul de a se opune prelucrării datelor și dreptul de a cere limitarea procesării acestora.

    </p>
    <p class="text-justify">10. Utilizatorul are dreptul să prezinte o plângere Președintelui Oficiului pentru Protecția
        Datelor cu Caracter Personal în legătură cu prelucrarea datelor personale ale Utilizatorului de către Operator.

    </p>
    <p class="text-sm-left">X. PREVEDERI FINALE

    </p>
    <p class="text-justify">1. Datele personale ale Clienţilor furnizate în timpul înregistrării în Magazin sunt
        prelucrate de către Wearco SA exclusiv în scopul realizării comenzilor; ele mai pot fi prelucrate şi în scopuri
        de marketing, dacă Clientul îşi exprimă acordul pentru acest lucru, într-o declaraţie separată. Clientul are
        dreptul de a completa, actualiza, corecta datele personale, de a retrage definitiv sau doar pentru o anume
        perioadă de timp permisiunea pentru prelucrarea lor sau de a cere ştergerea lor dacă sunt incomplete, neactuale,
        neadevărate sau au fost colectate prin încălcarea legii sau nu sunt neapărat necesare pentru atingerea scopului
        pentru care au fost colectate, precum şi dreptul de a se împotrivi în cazul prelucrării datelor personale în
        scopuri de marketing. Aceste informații sunt detaliate în Politica de Confidențialitate care este o completarea
        la prezentul Regulament.

    </p>
    <p class="text-justify">2. Neacceptarea prevederilor prezentului Regulament face imposibilă achiziţionarea
        mărfurilor oferite de Magazin. Magazinul oferă Clientului posibilitatea de a lua la cunoştinţă prevederile
        Regulamentului, la plasarea comenzii. Clienţii care deţin cont în Magazin vor fi informaţi cu privire la
        modificarea Regulamentului prin e-mail. Clientul care nu acceptă modificările aduse la Regulament are dreptul de
        a-şi închide contul în orice moment.

    </p>


</div>
<?php include '../footer.php' ?>
</body>
</html>