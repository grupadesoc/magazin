<!DOCTYPE html>
<html lang="en">
<head>
    <?php include '../../bootstrap/bootstrap.php'; ?>
    <?php include'../header.php'; ?>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .card {
            border-right-width: 0px;
            border-top-width: 0px;
            border-left-width: 0px;
            border-bottom: 0.5px;
            background-color: #FFFFFF;
        }

        #accordion {
            background-color: #FFFFFF
        }

        .card-header {
            background-color: #FFFFFF;
        }
    </style>
</head>
<body>
<div class="container">
    <div>
        <h2 class="text-center">REGULILE CARE GUVERNEAZĂ PROCESAREA DATELOR PERSONALE DE CĂTRE WEARCO LIMITED LIABILTY COMPANY</h2>
    </div>
    <div>
        <h3 class="text-center">INFORMATII GENERALE</h3>
        <p>Suntem foarte atenți când vine vorba de protejarea datelor cu caracter personal în conformitate cu legile în
            vigoare, în acord cu Regulamentul General de Protecție a Datelor Personale din 27 aprilie 2016 ("RPD").
            Obiectivul nostru este să îți furnizăm informații și control asupra procesării datelor tale și uneltelor
            care îți permit să te folosești de protecția drepturilor care rezultă din lege.</p>
        <p>În rândurile de mai jos vă prezentăm informații despre cum procesăm datele personale, cum le securizăm și cu
            cine le împărțim. Daca aveți întrebări suplimentare despre cum vă folosim datele personale, contactați-ne la
            următoarea adresă de e-mail: dpo@wearco.pl.</p>
    </div>
    <div id="accordion">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                            aria-controls="collapseOne">Cum va obtinem datele?
                    </button>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    Vă folosim datele personale deoarece ați decis să faceți cumpărături pe magazinul nostru online
                    www.answear.ro ("Magazin") sau pentru că v-ați dat acordul să primiți informații comerciale prin
                    adresa de e-mail furnizată sau ne-ați dat acordul să folosim adresa prin utilizarea websiteului
                    Magazinului. Magazinul operează în conformitate cu Regulamentul
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                            aria-expanded="false" aria-controls="collapseTwo">
                        Cine este administratorul datelor voastre personale?
                    </button>
                </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    Magazinul online answear.com este condus de "Wearco SA". Firma este înscrisă la registrul comerțului
                    KRS prin Judecătoria pentru sectorul Cracovia Centru, al XI-lea Departament Economic al Registrului
                    Național Judecătoresc sub numărul KRS 0000816066// Wearco SA cu sediul în Cracovia (31-564),str.
                    Aleja Pokoju 18, înscrisă în Registrul firmelor al Registrului Naţional Judiciar de pe lângă
                    Tribunalul Districtual Cracovia - Śródmieście din Cracovia, secţia a XI-a Economică, sub nr. KRS
                    0000816066
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingThree">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"
                            aria-expanded="false" aria-controls="collapseThree">
                        CUM POT CONTACTA INSPECTORUL DATELOR PERSONALE?
                    </button>
                </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    Contactează Inspectorul nostru al Datelor Personale: dpo@wearco.pl OR Data Protection Inspector
                    Wearco SA Aleja Pokoju 18 31-564 Kraków


                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingFour">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour"
                            aria-expanded="false" aria-controls="collapseFour">
                        Cum va procesam datele personale
                    </button>
                </h5>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                <div class="card-body">
                    <p>Dacă folosiți Magazinul nostru, vă vom procesa datele personale pentru scopurile următoare:
                    </p>
                    <p>a) Pentru a respecta contractul de vânzare stabilit între dvs. și Magazin - fundamentul pentru
                        procesarea datelor dvs., care rezulta în urma acceptării termenilor și condițiilor Magazinului.
                        Cea mai mare parte a datelor pe care ni le furnizați sunt necesare pentru a implementa cu succes
                        contractul de vânzare și livrarea bunurilor cumpărate către dvs.; furnizarea datelor personale
                        pentru acest scop nu este obligatorie, dar este necesară pentru a respecta contractul.
                    </p>
                    <p>b) Pentru a vă administra contul de pe site-ul Magazinului este necesar să vă creați un cont de
                        utilizator, acceptând astfel termenii și condițiile Magazinului. Crearea unui cont în Magazin vă
                        va permite, de asemenea, să vă accesați datele pe care le-ați furnizat, inclusiv istoria
                        cumpărăturilor, și să vă folosiți de unele drepturi legate de procesarea datelor; furnizarea
                        datelor personale pentru acest scop nu este obligatorie, dar este necesară pentru a respecta
                        contractul.
                    </p>
                    <p>c) Pentru a procesa plângeri - la baza acestei procesări a datelor stă obligația
                        Administratorului care rezultă din prevederea legii legată de garanția pentru defectele găsite
                        în obiectele vândute. Furnizarea datelor în formularul plângerii este obligatorie pentru o
                        adecvată considerare a plângerii dvs.
                    </p>
                    <p>d) Dacă vă exprimați consimțământul separat, vom trimite informații comerciale despre bunurile
                        oferite spre vânzare în Magazin pe adresa de e-mail furnizată de dvs., inclusiv ofertele
                        promoționale - în acest caz, baza de prelucrare a datelor este consimțământul dumneavoastră,
                        care nu este obligatorie și o puteți retrage oricând contactând datele de mai sus sau făcând
                        click pe linkul pe care îl trimitem în fiecare e-mail cu informații comerciale. Retragerea
                        consimțământului nu afectează corectitudinea procesării datelor în perioada anterioară
                        retragerii.
                    </p>
                    <p>e) Dacă vă exprimați consimțământul separat, vă vom trimite informații comerciale la numărul de
                        telefon mobil furnizat de dvs. către Magazin, inclusiv ofertele promoționale - în acest caz,
                        baza pentru prelucrarea datelor este consimțământul dvs., care nu este obligatoriu și îl puteți
                        retrage în orice moment, contactând-ne prin datele de mai sus. Retragerea consimțământului nu
                        afectează corectitudinea procesării datelor în perioada anterioară retragerii.
                    </p>
                    <p>f) Pentru a vă trimite mesaje de marketing personalizate pe site-ul magazinului, de ex. care iau
                        forma unei sugestii de a cumpăra bunuri cu ajutorul profilării. Mesajele vor fi pregătite pe
                        baza analizei achizițiilor dvs. - baza de prelucrare a datelor dvs. în acest caz, va fi
                        interesul legitim al Administratorului constând în comercializarea bunurilor oferite în magazin,
                        atât bunurile Administratorului, cât și ale furnizorilor.
                    </p>
                    <p>g) În scopuri de marketing - din când în când, putem trimite informații despre oferta magazinului
                        Answear.ro sau despre oferta partenerilor noștri comerciali la adresa de livrare furnizată de
                        dvs.. Baza de prelucrare a datelor dvs. în acest sens va fi interesul legitim al partenerilor
                        noștri în comercializarea produselor indicate în ofertă. Puteți oricând obiecta procesării
                        datelor dvs. și nu vom mai face acest lucru. Vă puteți exprima opoziția contactând-ne sau
                        folosind formularul de aici.
                    </p>
                    <p>h) În scopuri statistice interne ale Administratorului - în acest caz, baza pentru prelucrarea
                        datelor va fi interesul legitim al Administratorului, constând în colectarea de informații
                        pentru a permite dezvoltarea afacerii și personalizarea serviciilor pentru nevoile
                        utilizatorilor Magazinului.
                    </p>
                    <p>i) Pentru a confirma îndeplinirea îndatoririlor noastre și pentru a investiga reclamațiile sau a
                        apăra împotriva revendicărilor care pot fi îndreptate împotriva noastră, prevenirea sau
                        detectarea fraudei - baza pentru prelucrarea datelor dvs. în acest caz va fi interesul legitim
                        al Administratorului, care este protecția drepturilor, confirmarea performanței și obținerea în
                        acest sens a remunerației datorate de clienții Administratorului.
                    </p>
                    <p>Acordăm atenție transparenței prelucrării datelor dvs. personale. Dacă aveți întrebări cu privire
                        la procesul sau regulile de prelucrare a datelor, vă rugăm să ne contactați.
                        Noi procesam datele dumneavoastra in conformitate cu legea, asigurandu-ne ca acestea sunt
                        actuale si corecte. Prin urmare, din când în când vă vom aminti despre necesitatea actualizării
                        datelor prin trimiterea unui mesaj la adresa de e-mail furnizată de dvs..
                        Datele dvs. personale nu vor fi procesate pentru luarea de decizii automate fără consimțământul
                        dumneavoastră.</p>


                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingFive">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive"
                            aria-expanded="false" aria-controls="collapseFive">
                        Este obligatorie furnizarea datelor personale?
                    </button>
                </h5>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body">
                    Este decizia dvs. dacă doriți să ne furnizați date și ce fel de date ne furnizați, dar rețineți că
                    atunci când faceți cumpărături în magazin, furnizarea anumitor date va fi obligatorie pentru
                    executarea contractului de vânzare, deoarece fără acestea nu vom putea procesa comanda.
                    Nefurnizarea datelor pe care vi le cerem rezultă în imposibilitatea plasării comenzii.
                    Nu este obligatoriu să vă dați consimțământul de a primi informații comerciale la adresa de e-mail
                    furnizată sau numărul de telefon furnizat pentru a realiza un acord de vânzare a bunurilor. Dacă vă
                    dați consimțământul, îl poți retrage oricând.
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingSix">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix"
                            aria-expanded="false" aria-controls="collapseSix">
                        Cu cine vom impartasi informatiile dvs. personale?
                    </button>
                </h5>
            </div>
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                <div class="card-body">
                    <p>Vom transmite datele dvs. entităților care colaborează cu noi pentru a îndeplini contractul de
                        vânzare a bunurilor achiziționate de dvs.:
                    </p>
                    <p>a) În funcție de metoda de livrare pe care o alegeți, vă vom împărți datele necesare livrării de
                        bunuri către una dintre următoarele entități:
                    </p>
                    <ul>
                        <li> Fan Courier
                        </li>

                        <li>Alte entități care vor furniza în viitor servicii de livrare pentru bunurile achiziționate
                            de dvs. în magazin.
                        </li>
                    </ul>
                    <p>b) În funcție de alegerea metodei de plată pentru bunurile achiziționate, vă vom împărtăși datele
                        necesare pentru a primi sau efectua plăți pentru bunurile achiziționate către următoarele
                        entități:
                    </p>
                    <ul>
                        <li> Una dintre companiile de curierat menționate mai sus, în cazul în care metoda de plată
                            pentru bunurile selectate a fost "numerar la livrare"

                        </li>

                        <li>Netopia Mobilpay - dacă ați ales sistemul de plăți Netopia Mobilpay ca metodă de plată,
                        </li>
                        <li>
                            Alți operatori de plată cu care vom coopera pentru a primi o plată pentru bunurile
                            achiziționate de dvs.
                        </li>
                    </ul>
                    <p>c) Dacă ați fost de acord să primiți informații comerciale pe adresa de e-mail sau pe numărul de
                        telefon furnizate de dvs., vă vom împărtăși datele cu entitățile care furnizează serviciul
                        nostru de solicitare a trimiterii informațiilor comerciale, cum ar fi:
                    </p>
                    <ul>
                        <li> • LINK Mobility Poland sp. z o. o.


                        </li>

                        <li>N• ExpertSender Sp. z o.o.

                        </li>
                    </ul>
                    <p>d) În plus, datele dvs. vor fi partajate cu entitățile care prelucrează datele personale ale
                        clienților noștri, în măsura în care este necesar pentru a găzdui site-urile magazinului: Atende
                        SA,
                    </p>
                    <p>e) De asemenea, vă putem împărtăși datele personale cu alte entități din categoriile de mai sus,
                        cu care vom stabili cooperarea.

                    </p>

                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingSeven">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven"
                            aria-expanded="false" aria-controls="collapseSeven">
                        Ce drepturi aveti in legatura cu prelucrarea datelor de catre noi?
                    </button>
                </h5>
            </div>
            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                <div class="card-body">
                    <p>În conformitate cu GDPR, aveți acces la un număr de drepturi în legătură cu transferul
                        informațiilor dvs. personale către noi, cum ar fi:
                    </p>
                    <p>a) Dreptul de a ști cum sunt prelucrate datele dvs. personale - dacă aveți întrebări cu privire
                        la modul cum procesăm datele dvs., vă rugăm să ne contactați utilizând formularul de contact
                        furnizat în această pagină sau prin trimiterea de informații la dpo@wearco.pl, va stam la
                        dispozitie,
                    </p>
                    <p>b) Dreptul de a accesa și de a actualiza datele - aveți întotdeauna acces la datele dvs.
                        personale din contul dvs. în magazin.Puteți edita și actualiza datele furnizate. Dacă nu ați
                        creat un cont în magazin, vă rugăm să ne contactați prin intermediul formularului de contact din
                        această pagină sau scrieți-ne la Biroul nostru pentru Protecția Datelor cu solicitarea de a vă
                        accesa datele - vă vom oferi informații despre datele dvs. și le vom actualiza la solicitarea
                        dvs.,
                    </p>
                    <p>c) În termenii stabiliți de GDPR, aveți, de asemenea, dreptul la:
                    </p>
                    <ul>
                        <li>Eliminarea datelor - dacă doriți ca noi să nu mai procesăm datele dvs., vă puteți șterge
                            contul în magazin sau ne puteți raporta o astfel de solicitare. Vă rugăm să rețineți că
                            acest lucru nu este un drept absolut și putem refuza să ștergem datele despre care avem o
                            bază pentru prelucrarea sa (de exemplu, îndeplinirea unei obligații legale sau urmărirea sau
                            apărarea împotriva cererilor care ar putea fi îndreptate împotriva noastră) .
                        </li>
                        <li>
                            Cereri de limitare a procesării datelor,
                        </li>
                        <li> Pentru a obiecta la prelucrarea datelor dvs., dacă baza procesării este un interes legitim
                            al administratorului sau care îndeplinește sarcini de interes public,

                        </li>
                        <li>
                            Retragerea consimțământului, dacă datele sunt procesate pe baza consimțământului dvs.,
                        </li>
                        <li>
                            Transfer de date, dacă procesarea se bazează pe un contract sau pe acordul dvs.
                        </li>
                    </ul>
                    <p></p>
                </div>
            </div>
        </div>
    </div>

    <p>Publicat: 30.12.2019</p>
</div>
</body>
</html>