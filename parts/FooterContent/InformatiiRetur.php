<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <?php include'../../bootstrap/bootstrap.php';
    include'../header.php'?>
</head>
<body>
<div class="container">
    <h2 class="text-center">RELAȚII CU CLIENȚII</h2>
    <h5 class="text-center">RETUR GRATUIT LA COMENZI DE PESTE 200 LEI SI PLASATE DIN CONTUL SHOP
    </h5>
    <h6 class="text-center"></h6>
    <p>Echipa ANSWEAR.ro isi doreste ca tu sa porti numai ceea ce ți se potrivește perfect, așa ca am facut procesul de
        retur ANSWEAR.ro foarte usor.
        Daca produsul pe care l-ai comandat nu ti se potriveste sau daca pur si simplu vrei sa il returnezi intrucat nu
        a corespuns asteptarilor tale, poti beneficia de retur gratuit daca valoarea produsul pe care dorești să îl
        returnezi este de min 200 lei.</p>
    <p>Cum procedezi pentru a beneficia de retur gratuit?

    </p>
    <p>1. Intră în contul tău de client de pe ANSWEAR.ro

    </p>
    <p>2. Accesează sectiunea „Retur”

    </p>
    <p>3. Apasă butonul „Returnează”

    </p>
    <p>4. Selectează produsul/produsele pe care dorești să le returnezi

    </p>
    <p>5. Completează datele specifice returului

    </p>

    <p>Comanda ta va fi inregistrată iar în aproximativ două zile un curier va ajunge la tine pentru ridicarea returului. Asigura-te ca ai pregatit comanda anterioara si ai impachetat-o. In cel mai scurt timp, daca totul este in regula cu returul tau, vei primi ramburs banii aferenti comenzii returnate. Suma aferentă returului va fi returnată în contul IBAN mentionat de tine in formularul de retur.

    </p>
    <p>Ține minte:
    </p>
    <ul>
        <li>Ești răspunzător pentru diminuarea valorii produsului ca urmare a utilizării lui într-un alt mod decat cel necesar pentru constatarea caracteristicilor produselor.</li>
        <li>Nu uita să atașezi originalul sau copia dovezii de cumpărare (bonul fiscal sau factura).</li>
        <li>Termenul returnării plății poate fi de până la 14 zile conform regulilor din legile în vigoare.</li>

    </ul>
</div>
<?php include '../footer.php'?>
</body>
</html>