<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php include '../../bootstrap/bootstrap.php';
    include '../header.php' ?>
    <title></title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col">
            <p>Shop.com înseamnă cumpărături fără limite. În oferta noastră vei găsi îmbrăcăminte, încălțăminte dar și
                accesorii ale celor mai moderne 300 de mărci din lume. Printre ele se află și mărci cunoscute și foarte
                populare (Mango, Vero Moda), mărci sport (Adidas, Nike, New Balance), mărci de jeans (Levi’s, Lee,
                Wrangler), precum și mărci high-end (Diesel, Guess Jeans, Tommy Hilfiger, Valentino, DKNY).</p>

            <p>Shop.com este primul magazin multibrand din Polonia care oferă o asemenea gamă largă de produse. La noi
                poți face cumpărături pentru întreaga familie pentru că oferta noastră cuprinde produse pentru femei,
                bărbați și copii. Repede, sigur și - cel mai important - fără să ieși din casă.

            </p>

            <p>În afară de mii de produse descrise în detaliu și fotografiate, clienții pot găsi pe site-ul nostru și
                sfaturi practice dar și informații despre cele mai noi tendințe. Lucrăm în mod constant cu trendsetteri,
                bloggeri și experți în modă oferind clienților inspirație fără limite pentru căutările și experimentele
                lor în domeniul modei.

            </p>
            <p>
                Shop.com a fost înființată în decembrie 2010 și a început să funcționeze în februarie 2011. După 3 ani
                de succes pe piața din Polonia (având un număr tot mai mare de clienți mulțumiți și de premii în
                branșă), în martie 2014 ANSWEAR.com și-a început activitatea în Cehia iar în noiembrie 2014 în Slovacia.


            </p>
        </div>
        <div class="col">
            <img style="s" src="https://answear.ro/rwd/images/aboutUsHang.jpg">
        </div>
    </div>
<div class="row">
    <h2>Viziteaza-ne pe:</h2>
    <a class="btn-floating btn-lg btn-fb" type="button" role="button"><i class="fab fa-facebook-f"></i></a>
    <a class="btn-floating btn-lg btn-tw" type="button" role="button"><i class="fab fa-twitter"></i></a>
    <a class="btn-floating btn-lg btn-gplus" type="button" role="button"><i class="fab fa-google-plus-g"></i></a>
    <a class="btn-floating btn-lg btn-ins" type="button" role="button"><i class="fab fa-instagram"></i></a>

</div>
</div>
<?php include '../footer.php'?>
</body>
</html>