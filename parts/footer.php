<!-- Footer -->
<!DOCTYPE html>
<html lang="en">
<head>

<!--    --><?php //include 'bootstrap/bootstrap.php' ?>
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div style="background-color: black; height:80px">
    <div class="container ">
        <div class="row" >
            <nav class="navbar navbar-expand-md bg-transparent navbar-dark" style="width: 100%">
                <!-- Brand -->
                <a class="navbar-brand" href="#"></a>

                <!-- Toggler/collapsibe Button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Navbar links -->
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control mr-sm-2" type="email" placeholder="Your email">
                                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Subscribe</button>
                            </form>
                        </li>
                        <li class="nav-item " style="margin-right: auto !important">
                            <a class="nav-link" href="#"> Invite friends for 15% discount</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<footer class="page-footer font-small indigo">

    <!-- Footer Links -->
    <div class="container text-center text-md-left">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-md-3 mx-auto">

                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Informatii</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="http://188.240.210.8/web-04/adrian/AdrianShop/parts/FooterContent/TermeniSiConditii.php">Termeni si Conditii</a>
                    </li>
                    <li>
                        <a href="http://188.240.210.8/web-04/adrian/AdrianShop/parts/FooterContent/PoliticaDeConfidentialitate.php">Politica de confidentialitate</a>
                    </li>
                    <li>
                        <a href="http://188.240.210.8/web-04/adrian/AdrianShop/parts/FooterContent/AboutUS.php">Despre noi</a>
                    </li>
                </ul>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 mx-auto">

                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Relatii cu clientii</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="http://188.240.210.8/web-04/adrian/AdrianShop/parts/FooterContent/InformatiiRetur.php">Informatii retur si reclamatii.</a>
                    </li>
                    <li>
                        <a href="#!">Modalitati de plata.</a>
                    </li>
                    <li>
                        <a href="#!">Metode de livrare.</a>
                    </li>
                    <li>
                        <a href="#!">Termenul de realizare a comenzii</a>
                    </li>
                </ul>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 mx-auto">

                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Metode de plata</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Modalitati de plata</a>
                    </li>
                </ul>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->
            <div class="col-md-3 mx-auto">

                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Metode de livrare</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Colet prin curierat FAN COURIER</a>
                    </li>
                </ul>
            </div>
            <hr/>           <!-- Grid column -->

        </div>
        <!-- Grid row -->
        <hr/>
    </div>
    <!-- Footer Links -->
</footer>
<!-- Footer -->
<!-- Footer -->
<footer class="page-footer font-small mdb-color lighten-3 pt-4">

    <!-- Footer Links -->
    <div class="container text-center text-md-left">

        <!-- Grid row -->
        <div class="row">


            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 mx-auto my-md-4 my-0 mt-4 mb-1">

                <!-- Contact details -->
                <h5 class="font-weight-bold text-uppercase mb-4">Address</h5>

                <ul class="list-unstyled">
                    <li>
                        <p>
                            <i class="fas fa-home mr-3"></i> New York, NY 10012, US</p>
                    </li>
                    <li>
                        <p>
                            <i class="fas fa-envelope mr-3"></i> info@example.com</p>
                    </li>
                    <li>
                        <p>
                            <i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>
                    </li>
                    <li>
                        <p>
                            <i class="fas fa-print mr-3"></i> + 01 234 567 89</p>
                    </li>
                </ul>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->

            <div class="col-md-2 col-lg-2 text-center mx-auto my-4">

                <!-- Social buttons -->
                <h5 class="font-weight-bold text-uppercase mb-4">Follow us!</h5>

                <!-- Facebook -->
                <a type="button" class="btn-floating btn-fb">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <!-- Twitter -->
                <a type="button" class="btn-floating btn-tw">
                    <i class="fab fa-twitter"></i>
                </a>
                <!-- Google +-->
                <a type="button" class="btn-floating btn-gplus">
                    <i class="fab fa-google-plus-g"></i>
                </a>
                <!-- Dribbble -->
                <a type="button" class="btn-floating btn-dribbble">
                    <i class="fab fa-dribbble"></i>
                </a>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="index.php"> Shop.com</a>
    </div>
    <!-- Copyright -->

</footer>
</body>
</html>