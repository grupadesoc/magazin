<?php include '../../functions.php'?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container-fluid">
<form class="form-horizontal" action="//../ProcessCreateUser.php" method="post">

        <!-- Form Name -->
        <h2>Register Yourself</h2>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="firstname">First Name</label>
            <div class="col-md-4">
                <input id="firstName" name="firstName" type="text" placeholder="John" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="lname">Last Name</label>
            <div class="col-md-4">
                <input id="lastName" name="lastName" type="text" placeholder="Doe" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-4">
                <input id="email" name="email" type="text" placeholder="" class="form-control input-md" required="">

            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="lname">Username:</label>
            <div class="col-md-4">
                <input id="username" name="username" type="text" placeholder="Doe" class="form-control input-md" required="">
            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-4">
                <input id="password" name="password" type="password" placeholder="" class="form-control input-md" required="">

            </div>
        </div>

        <!-- Button (Double) -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="save"></label>
            <div class="col-md-8">
                <button id="save" name="save" class="btn btn-success">Register</button>
                <button id="clear" name="clear" class="btn btn-danger">Reset</button>
            </div>
        </div>

</form>
</div>