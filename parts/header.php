<!DOCTYPE html>
<html lang="en">
<head>
<!--    --><?php //include 'bootstrap/bootstrap.php' ?>
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta charset="UTF-8">
    <title>Shop</title>
 </head>
<body>
<div>
    <div>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark hidden-xs " style="height: 25px">
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse hidden-xs" id="navbarCollapse">
                <div class="navbar-nav mx-auto font-weight-bold hidden-xs">
                    <a href="#" class="nav-item nav-link ">Livrare Gratuita</a>
                    <a href="#" class="nav-item nav-link">30 de zile pentru retur</a>
                    <a href="#" class="nav-item nav-link">50% pentru inscrierea la newsletter</a>
                    <a href="#" class="nav-item nav-link">100% produse originale</a>
                </div>
            </div>
        </nav>
    </div>
    <div>
        <div>
            <nav class="navbar navbar-expand-lg navbar-light  ">
                <a class="navbar-brand font-weight-bold " href="index.php"><h1>Shop</h1></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                        aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse " id="navbarTogglerDemo02" style="margin: 30px 40px;">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 font-weight-bold" style="font-size: 20px">
                        <li class="nav-item">
                            <a class="nav-link " href="#">Femei</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Barbati</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Copii</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Link</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav mr-8 mt-2 mt-lg-0 font-weight-bold ">
                        <li class="nav-item active">
                            <a class="nav-link " href="parts/login/login.php"><i class="fas fa-user-alt" style="font-size: 30px"></i></a>
                            <a class="nav-link " href="parts/login/login.php">Log in</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="wishlist.php"> <i class="fas fa-star " style="font-size: 30px"></i></a>
                            <a class="nav-link" href="wishlist.php">Wishlist</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="cos.php"><i class="fas fa-shopping-cart " style="font-size: 30px"></i></a>
                            <a class="nav-link" href="cos.php">Cos</a>
                        </li>
                    </ul>


                    <button type="button" class="btn btn-transparent" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="fas fa-search" style="font-size: 30px"></i></button>

                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <form class="form-inline md-form form-sm mt-0">
                                    <i class="fas fa-search"  aria-hidden="true"></i>
                                    <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search"
                                           aria-label="Search">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </nav>
    </div>
</div>
<hr/>
<div>
    &nbsp; <a href="index.php">Acasa</a> > <a href="#">Favorites</a> > <a href="#">Femei</a> > <a href="#">Imbracaminte</a> > <a href="#">Geci si Paltoane</a>
</div>
<hr/>
</body>