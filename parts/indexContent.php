<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php include 'bootstrap/bootstrap.php' ?>
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Title</title>
</head>
<body>
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" style="height: 600px">

            <div class="carousel-item active">
                <img class="d-block w-100"
                     src="https://cdn.livekindly.co/wp-content/uploads/2019/09/vegan-plant-based-news-vegan-fashion-week-livekindly.jpg"
                     alt="First slide">

            </div>
            <div class="carousel-item">
                <img class="d-block w-100"
                     src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/hbz-fall-color-index-1566919996.jpg?crop=0.492xw:0.984xh;0.253xw,0&resize=640:*"
                     alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100"
                     src="https://images.unsplash.com/photo-1515886657613-9f3515b0c78f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                     alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <div class="container">
        <div class="row top-buffer">
            <div class="col-md-4 hover-fade padding-0"><a href="#" title="">
                <img src="http://placeimg.com/260/260/nature/2/" alt=""/>
                <div class="carousel-caption">
                    <h1>Example headline.</h1>
                </div>
            </a></div>
            <div class="col-md-4 hover-fade padding-0"><a href="#" title="">
                <img class="img-fluid" src="http://placeimg.com/260/260/nature/2/" alt=""/>
                <div class="carousel-caption">
                    <h1>Last minute!</h1>
                    <h4>Rochie de revelion</h4>
                </div>
            </a></div>
            <div class="col-md-4 hover-fade padding-0"><a href="#" title="">
                <img src="http://placeimg.com/260/260/nature/2/" alt=""/>
                <div class="carousel-caption">
                    <h1>Example headline.</h1>
                </div>
            </a></div>

        </div>
        <div class="row top-buffer ">
            <div class="col-md-4 hover-fade padding-0"><a href="#" title="">
                <img src="http://placeimg.com/260/260/nature/2/" alt=""/>
                <div class="carousel-caption">
                    <h1>Vezi colectia</h1>
                </div>
            </a></div>
            <div class="col-md-4 hover-fade padding-0"><a href="#" title="">
                <img src="http://placeimg.com/260/260/nature/2/" alt=""/>
                <div class="carousel-caption">
                    <h1>Premium</h1>
                </div>
            </a></div>
            <div class="col-md-4 hover-fade padding-0"><a href="#" title="">
                <img src="http://placeimg.com/260/260/nature/2/" alt=""/>
                <div class="carousel-caption">
                    <h1>Toate modelele</h1>
                </div>
            </a></div>
        </div>
    </div>
    <div class="container">
        <h1 style="color: slategray">Gifts</h1>
        <div class="card-deck">
            <div class="card">
                <img class="card-img-top" src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/unique-christmas-gifts-to-pleasantly-surprise-your-family-and-friends-1575580721.png?crop=0.502xw:1.00xh;0,0&resize=640:*" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">
                        <big class="text-muted">Last updated 3 mins ago</big>
                    </p>
                </div>
            </div>

            <div class="card">
                <img class="card-img-top" src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/tc-holidaygifts-1540491674.jpg?crop=1.00xw:1.00xh;0,0&resize=480:*" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">
                        <big class="text-muted">Last updated 3 mins ago</big>
                    </p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="https://www.onegoodthingbyjillee.com/wp-content/uploads/2018/12/Gifts-in-a-Jar-23.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">
                        <big class="text-muted">Last updated 3 mins ago</big>
                    </p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="https://allmehandidesigns.com/wp-content/uploads/2019/10/91piLxVgl2L._SL1500_.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">
                        <big class="text-muted">Last updated 3 mins ago</big>
                    </p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="https://i.pinimg.com/originals/76/d9/ba/76d9ba00379273d7897ae81963128c9e.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
        </div>
        <h1 style="color: slategray">Trenduri de iarna</h1>
        <div class="card-deck ">
            <div class="card">
                <img class="card-img-top" src="https://cdn.cliqueinc.com/posts/283418/winter-fashion-trends-2019-283418-1572294546534-image.700x0c.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>

            <div class="card">
                <img class="card-img-top" src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/hbz-winter-shoe-00-index-1571168619.jpg?crop=0.502xw:1.00xh;0.251xw,0&resize=640:*" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="https://cache.net-a-porter.com/content/images/story-body-content-v2-0-1534156311229.jpeg/w1900_q65.jpeg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="https://i1.wp.com/www.thedollsfactory.com/wp-content/uploads/2018/03/Fall-winter-2018-2019-Trends-Flowers-elie-saab-1.jpg?resize=1170%2C1755" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="https://strawberriesnchampagne.com/wp-content/uploads/2018/01/Highlights_Wintertrends_Bild11.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">
                        <small class="text-muted">Last updated 3 mins ago</small>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" style="background-color: #999999">
        <div class="container">
            <div><h1 class="text-center">#WeAreAwesome</h1>
                <h4 class="text-center">Fa parte din echipa noastra!Afiseaza-ti stilul pe instagram!</h4>
            </div>
            <div class="card-deck">

                <div class="card bg-transparent text-white" style="border: none;">
                    <a class="hover-fade"> <img class="card-img-top "
                                                src="https://cbsnews1.cbsistatic.com/hub/i/2018/11/06/0c1af1b8-155a-458e-b105-78f1e7344bf4/2018-11-06t054310z-1334124005-rc1be15a8050-rtrmadp-3-people-sexiest-man.jpg"
                                                alt="Card image cap"> </a>
                    <p class="text-center">Idris Elba</p></div>
                <div class="card bg-transparent text-white" style="border: none;">
                    <a class="hover-fade"> <img class="card-img-top "
                                                src="https://cbsnews1.cbsistatic.com/hub/i/2018/11/06/0c1af1b8-155a-458e-b105-78f1e7344bf4/2018-11-06t054310z-1334124005-rc1be15a8050-rtrmadp-3-people-sexiest-man.jpg"
                                                alt="Card image cap"> </a>
                    <p class="text-center">Idris Elba</p></div>
                <div class="card bg-transparent text-white" style="border: none;">
                    <a class="hover-fade"> <img class="card-img-top "
                                                src="https://cbsnews1.cbsistatic.com/hub/i/2018/11/06/0c1af1b8-155a-458e-b105-78f1e7344bf4/2018-11-06t054310z-1334124005-rc1be15a8050-rtrmadp-3-people-sexiest-man.jpg"
                                                alt="Card image cap"> </a>
                    <p class="text-center">Idris Elba</p></div>
                <div class="card bg-transparent text-white" style="border: none;">

                    <a class="hover-fade"> <img class="card-img-top "
                                                src="https://cbsnews1.cbsistatic.com/hub/i/2018/11/06/0c1af1b8-155a-458e-b105-78f1e7344bf4/2018-11-06t054310z-1334124005-rc1be15a8050-rtrmadp-3-people-sexiest-man.jpg"
                                                alt="Card image cap"> </a>
                    <p class="text-center">Idris Elba</p></div>
            </div>
            <div class="card-deck">

                <div class="card bg-transparent text-white" style="border: none; width:20%">
                    <a class="hover-fade"> <img class="card-img-top "
                                                src="https://cbsnews1.cbsistatic.com/hub/i/2018/11/06/0c1af1b8-155a-458e-b105-78f1e7344bf4/2018-11-06t054310z-1334124005-rc1be15a8050-rtrmadp-3-people-sexiest-man.jpg"
                                                alt="Card image cap"> </a>
                    <p class="text-center">Adrian</p></div>
                <div class="card bg-transparent text-white" style="border: none; width: 20%">
                    <a class="hover-fade"> <img class="card-img-top "
                                                src="https://cbsnews1.cbsistatic.com/hub/i/2018/11/06/0c1af1b8-155a-458e-b105-78f1e7344bf4/2018-11-06t054310z-1334124005-rc1be15a8050-rtrmadp-3-people-sexiest-man.jpg"
                                                alt="Card image cap"> </a>
                    <p class="text-center">Adrian</p></div>
                <div class="card bg-transparent text-white" style="border: none; width: 20%">
                    <a class="hover-fade"> <img class="card-img-top "
                                                src="https://cbsnews1.cbsistatic.com/hub/i/2018/11/06/0c1af1b8-155a-458e-b105-78f1e7344bf4/2018-11-06t054310z-1334124005-rc1be15a8050-rtrmadp-3-people-sexiest-man.jpg"
                                                alt="Card image cap"> </a>
                    <p class="text-center">Adrian</p>
                </div>
                <div class="card bg-transparent text-white" style="border: none; width: 20%">
                    <a class="hover-fade"> <img class="card-img-top "
                                                src="https://cbsnews1.cbsistatic.com/hub/i/2018/11/06/0c1af1b8-155a-458e-b105-78f1e7344bf4/2018-11-06t054310z-1334124005-rc1be15a8050-rtrmadp-3-people-sexiest-man.jpg"
                                                alt="Card image cap"> </a>
                    <p class="text-center">Adrian</p></div>
            </div>

            <div class="text-center " style="padding:20px">
                <button type="button" class="btn btn-dark"><a style="font-size: 25px">Vezi mai multe</a></button>
            </div>
        </div>
    </div>

</body>
</html>