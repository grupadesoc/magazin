<?php

 include '../functions.php';
include  'header.php';
$category= new Category($_GET['id']);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php include '../bootstrap/bootstrap.php' ?>
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Title</title>
</head>
<body>
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" href="#">ASCUNDE FILTRELE</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link active" href="#">SORTEAZA</a>
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">CELE MAI NOI</a>
                    <a class="dropdown-item" href="#">CELE MAI SCUMPE</a>
                    <a class="dropdown-item" href="#">CELE MAI IEFTINE</a>
                    <a class="dropdown-item" href="#">LA REDUCERE</a>
                    <a class="dropdown-item" href="#">POPULARITATE</a>
                    <div class="dropdown-divider"></div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link active" href="#">ARATA</a>
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">60</a>
                    <a class="dropdown-item" href="#">180</a>
                    <div class="dropdown-divider"></div>
                </div>
            </li>
        </ul>

        <div class="row">

            <?php foreach ($category->getProducts() as $product): ?>

                <div class="col-2" align="left">

                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">           <?php echo $product->name; ?></h5>
                        <p class="card-text"><img src="../Poze/<?php echo $product->image;?>"/></p>

                        <a href="#" class="btn btn-primary"><?php echo $product->price; ?> RON</a>
                    </div>
                </div>
            </div>
    <?php endforeach; ?>
        </div>
        <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#">Next</a>
                </li>
            </ul>
        </nav>
        <?php
        include 'footer.php'
?>